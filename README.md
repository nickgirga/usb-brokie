# usb-brokie
### A simple python script that makes it easy to determine when ADB and/or fastboot detects your Android device if the charging port is a bit broken.

# Usage
`usb-brokie` is a simple script with no dependencies aside from Python3, so you don't need to install it (though feel free to move it to your `/usr/bin` folder for easy access anywhere in a terminal). It only takes 2 commands: `fastboot` and `adb`. The syntax for it is `usb-brokie <command>`. So, for example, if I was trying to determine when my computer would see my phone when trying to lock my bootloader, I would use `usb-brokie fastboot`. If I was trying to determine when my computer would see my phone when trying to export my Godot game, I would use `usb-brokie adb`. Generally, you will be using `fastboot` if the device is not yet booted into Android, and you will use `adb` if it is fully booted into Android.

If using a system that allows you to interpret foreign files with 3rd-party interpreters such as python, you should be able to run it using `./usb-brokie <command>` while inside of the directory that contains the script. If your system does not support this, you can run it using `python3 usb-brokie <command>`. If you have copied it to your `/usr/bin` folder, you should be able to run it as just `usb-brokie <command>`.

Now, simply plug your device in, run `usb-brokie` in your desired mode, and play with your charging port until fastboot or adb sees it. `usb-brokie` will report a device if found and it will exit. When this happens, make sure not to move your phone. Even subtle movements may cause it to disconnect. Run `usb-brokie` an additional time after your phone is settled in a stable spot where the cable won't move to verify it is still connected.

# Disclaimer
This is not intended to fix your charging port. **By using this script, you agree to take full responsibility for any damage done to your device.** It is entirely possible that `usb-brokie` determines that your device is connected and your device may disconnect in the middle of flashing vital information to your device. Repair your charging port before attempting to flash anything that may brick it.
